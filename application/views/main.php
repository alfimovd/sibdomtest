<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $title; ?></title>

<link href="<?php echo URL::base(); ?>public/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo URL::base(); ?>public/css/style.css" rel="stylesheet" type="text/css" />

<meta name="description" content="<?php echo $description; ?>" />
</head>
 
<body>
	<nav class="navbar-inverse navbar-fixed-top navbar" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="/"><?php echo $title; ?></a>
			</div>
			<div class="collapse navbar-collapse">
				<ul id="w1" class="navbar-nav navbar-right nav">
					<li><a href="#">Алфимов Дмитрий</a></li>
					<li class="active"><a href="/">Главная</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="container">
		<div id="content">
	    	<?php echo $content; ?>
	    </div>
	</div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo URL::base(); ?>public/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo URL::base(); ?>public/js/script.js"></script>
</body>
</html>