<div class="row">
	<?php if( count($comments)>0 ) { ?>
		<?php foreach ($comments as $comment) { ?>
			<div class="comment">
				<blockquote>
				    <p><?= $comment->text ?></p>
				    <footer>
				    	<?= $comment->name ?>
				    	<?= Rating::instance()->render($comment->id); ?>
				    </footer>
				</blockquote>
			</div>
		<?php } ?>
	<?php } else { ?>
		<p>Здесь пока ни одной записи</p>
	<?php } ?>
</div>
<div class="row">
	<div class="panel panel-default">
	    <div class="panel-heading">
	    	<h3 class="panel-title">Ваше мнение</h3>
	    </div>
	    <div class="panel-body">
	    	<form role="form" action="/" method="post">
			    <div class="form-group <?php if(isset($errors['name'])) echo 'has-error'; ?>">
			    	<input type="text" class="form-control" placeholder="Ваше имя" name="name" value="<?php if(isset($request['name'])) echo $request['name']; ?>">
			    	<span class="help-block"><?php if(isset($errors['name'])) echo $errors['name']; ?></span>
			    </div>
			    <div class="form-group <?php if(isset($errors['text'])) echo 'has-error'; ?>">
			    	<textarea class="form-control" rows="3"  placeholder="Ваш комментарий" name="text"><?php if(isset($request['text'])) echo $request['text']; ?></textarea>
			    	<span class="help-block"><?php if(isset($errors['text'])) echo $errors['text']; ?></span>
			    </div>
			    <button type="submit" class="btn btn-default">Отправить</button>
			</form>
	    </div>
	</div>
</div>