<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Comment extends Controller_Common {
	
	public $template = 'main';

	public function action_index()
	{
		$request = [];
		$errors = [];
		if($_POST)
	    {
	 		$request = $_POST;
	    	$_POST = Arr::map('trim', $_POST);

	        $name = trim($_POST['name']);
	        $text = trim($_POST['text']);
			$text = htmlspecialchars(strip_tags($text));

	        $post = Validation::factory($_POST);
	        $post -> rule('name', 'not_empty')
				  -> rule('text', 'not_empty');
	 
	        if( $post->check() )
	        {
	        	$comment = new Model_Comment;
	        	$comment->name = $name;
				$comment->text = $text;
				$comment->save();
	 			$request = [];
	        }
	        else
	        {
	            $errors = $post->errors('validation');
	        }
	    }

		$comments = ORM::factory('Comment')->find_all();

		$content = View::factory('/comment/index', [
						        	"comments" => $comments,
						        	"request" => $request,
						        	"errors" => $errors,
						        ]);
        
        $this->template->title = 'Тестовое задание СибДом';
        $this->template->description = 'Страница со списком комментариеев';

        $this->template->content = $content;
	}

} // End Welcome
