<?php defined('SYSPATH') or die('No direct script access.');

return array(
	'view'            => 'rating/bootstrap',
	'rating_table'    => 'rating',
	'rating_table_ip' => 'rating_ip'
);