<div class="rating btn-group btn-group-xs" id="<?php echo $id; ?>">
    <button type="button" class="minus btn btn-default" data-type='minus'>
    	<span class="glyphicon glyphicon-minus"></span>
    </button>
    <div id="rating_<?php echo $id; ?>" class="value btn btn-default"><?php echo $rating; ?></div>
    <button type="button" class="plus btn btn-default" data-type='plus'>
    	<span class="glyphicon glyphicon-plus"></span>
    </button>
</div>