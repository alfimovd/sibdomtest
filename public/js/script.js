
var site = {
	init: function(){
		site.rating.rating_listener();
	},
	rating: {
		rating_listener: function(){
			$('.minus, .plus').click(function(){
				var mark = $(this).attr('data-type');
				var id = $(this).parent('.rating').attr('id');
				if(mark == 'minus')
					site.rating.set_rating(id, -1);
				else if(mark == 'plus')
					site.rating.set_rating(id, 1);
			});
		},
		set_rating: function(id, rating_mark)
		{
			$.ajax({
				type: "GET",			
				url: "/rating/"+id,
				data: ({rating_id : id, rating_mark : rating_mark}),
				dataType: 'json',	
				success: function(data){
					$('#rating_'+id).text(data);
				}
			});
		},
	},
}

$(document).ready(function(){
	site.init();
});	
